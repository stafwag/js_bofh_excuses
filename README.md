# Js Bofh Excuses

## Description

Bofh Excuses in javascript, useful if you need an excuse but don't have a webserver with CGI support at hand.

## Getting started

```html
<script type="text/javascript" src="./excuses.js"></script>

<script type="text/javascript">
document.writeln(bofhExcuses[calRand(bofhExcuses.length)]);

</script>
```

## Examples
### Output

<script type="text/javascript" src="./excuses.js"></script>
<strong>Your excuse:</strong></p>
<script type="text/javascript">// <![CDATA[
                        document.writeln(bofhExcuses[calRand(bofhExcuses.length)]);
// ]]></script>

### Exmaple html file

[excuse.html](./excuse.html)



